# Who Run The World app? CATs or DOGs ?

By **Olivier GUANDALINI** & **Hadrien GOBIER**

A simple distributed application running across multiple Docker containers. **The PDF file is also available, but I strongly recommand you to follow this README.md**.

<p align="center">
  <img src="Images/LOGO project.png" alt="Logo projet DevOps" />
</p>

## Table of content

[[_TOC_]]

## Requirement

- The project need to be done inside a virtual machine that run docker and docker compose. Follow the docker documentation as we saw during the course for installation instructions if you don't have it yet.

## Getting started

This solution uses Python, Node.js, .NET, with Redis for messaging and Postgres for storage.

## Architecture

![Architecture diagram](architecture.png)

- A front-end web app in [Python](/vote) which lets you vote between two options
- A [Redis](https://hub.docker.com/_/redis/) which collects new votes
- A [.NET](/worker/) worker which consumes votes and stores them in…
- A [Postgres](https://hub.docker.com/_/postgres/) database backed by a Docker volume
- A [Node.js](/result) web app which shows the results of the voting in real time

## Notes

The `WhoRunTheWorld` application only accepts one vote per client browser. It does not register additional votes if a vote has already been submitted from a client.

This isn't an example of a properly architected perfectly designed distributed app... it's just a simple
example of the various types of pieces and languages you might see (queues, persistent data, etc), and how to
deal with them in Docker at a basic level.

## Requirements

> __Nodes Naming convention__ :
We added the vagrantfile to deploy the current infrastructure of this project, so that you can emulate from your local PC the app project. Of course, you need to have vagrant installed on your VM & a PC with at least 12 or 16Gb of RAM.

> __Nodes Naming convention__ :
This project involve deploying a kubernetes cluster and the VMs that you use **MUST** be public vms. Feel free to une a local runner that can access your local VMs.
You MUST create 3 virtual machines. One named `[last-name]-master` and the other ones `[last-name]-worker1` and `[last-name]-worker2`

> __Required packets__ :
On top of that, you must have the following requirements :
* The git packet install
* Access to the repository to git clone through SSH or HTTPS
* Kubernetes, docker & docker-compose installed on the VMs. Follow this [tutorial](https://www.cherryservers.com/blog/install-kubernetes-on-ubuntu) to install kubernetes on Ubuntu 22.04.

> __Kubernetes cluster__ :
In order for your k8s cluster to run smoothly, verify the following things before advancing any further :

* Verify that your master and worker nodes are marked as ``Ready``.

![Master and worker nodes status](Images/Master%20and%20worker%20nodes%20status.png)

* Verify that your pods in all namespaces are up & running.

![Listing of pods](Images/Listing%20of%20pods.png)

Once your set up meets these requirements, you can start the deployment of the project.

> __Ports opening on VM__ :
Make sure to open all the required ports on your VMs.

# How to use the project

## 1. Dockerize the app

To use the application, you must clone the current repository on your master node with the git clone command. 
Then, you do a ``docker compose up -d`` to download the images and build the containers from it.

Then, login to your dockerhub registry with ``docker login`` and push the images to the repository of yours. So that when we deploy the app on k8s, the pods will pull from this repository.

![dockerhub registry UI](Images/dockerhub%20registry.png)

## 2. YAML manifests k8s

To deploy the dockerized voting app on the k8s cluster (which contains one master node and two worker nodes), you must have all the yaml files listed in the **k8s-manifests** folder. These yaml files will deploy the necessary pods with the docker images stored in the private docker registry.

## 3. Launching of the app with k8s

To launch the app inside k8s pods, type the following command : ``kubectl create -f k8s-manifests/``

It will launch all the required pods in the current namespace. The ``vote web`` app is then available on port ``31000`` on each host of the cluster. The ``result web`` app is available on port ``31001``.

To remove them, run:

```bash
kubectl delete -f k8s-manifests/
```

## 4. Accessing the frontend

Once your k8s deployment is done, you can access the frontend UI at the master node IP and at the port 30001. In our case, ``http://192.168.56.10:30001/`` :

![Frontend UI](Images/frontend.png)

## 5. CI/CD

To test our app with a CI/CD tool, we are going to use the CI/CD pipeline of the gitlab project. 
The first step is to register the runner from our master node with the following bash command :
```bash
# Create the docker container for the runner
docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest

# Launch of the runner container
docker exec -it gitlab-runner gitlab-runner register
```
We have the following output :

![Output registered runner](Images/Runner%20gitlab.png)

We can see that the runner has been successfuly registered on the project :

![Runner project](Images/Register%20runner.png)

Then, the final step consists of deploying the pipeline with the ``.gitlab-ci.yml`` file. You can monitor the advancement of the pipeline as follow :

![Pipeline](Images/Pipeline.png)